#ifndef IMAGE_TRANSFORMER_ROTATE_H
#define IMAGE_TRANSFORMER_ROTATE_H

struct image rotate_image(struct image image, int angle);

#endif //IMAGE_TRANSFORMER_ROTATE_H
