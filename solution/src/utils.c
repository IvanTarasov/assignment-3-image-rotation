#include "../include/utils.h"

#define ARGS_COUNT_MIN 4

const char* io_errors[5] = {
        "",
        "Could not read pixel!",
        "Non-24 bit BMP files are not supported!",
        "Could not read BMP header!",
        "You have opened not a BMP file!",
};
int error_read_check(enum read_status readStatus) {
    if (readStatus == READ_OK)
        return 1;
    printf("%s", io_errors[readStatus]);
    return 0;
}

int error_write_check(enum write_status writeStatus) {
    if (writeStatus == WRITE_ERROR) {
        fprintf(stderr, "Could not write data to BMP file!");
        return 0;
    }
    return 1;
}

int arguments_valid(int argc, char* args[]) {
    if (argc >= ARGS_COUNT_MIN) {
        int angle = atoi(args[3]);
        if (angle < 0 && angle >= -270) {
            angle += 360;
        } else if (labs(angle) > 270 || angle % 90 != 0) {
            fprintf(stderr, "Angle must be equal to 0, +-90, +-180, +-270");
            exit(1);
        }
        return angle;
    }
    fprintf(stderr, "Usage: ./image-transformer <source-image> <transformed-image> <angle>");
    exit(1);
}

FILE* file_open(char* file_name, char* mode) {
    FILE* file = fopen(file_name, mode);
    if (file == NULL) {
        fprintf(stderr, "Could not open file \"%s\"! Check file name, path or its rights.", file_name);
        exit(1);
    }
    return file;
}

void file_close(FILE* file) {
    if (fclose(file) != 0) {
        fprintf(stderr, "Could not close file!");
        exit(1);
    }
}
