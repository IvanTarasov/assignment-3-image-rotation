#include "../include/rotate.h"
#include "../include/bmp.h"
#include "../include/image.h"

int main(int argc, char* args[]) {
    int angle = arguments_valid(argc, args);

    FILE* file = file_open(args[1], "rb");
    struct image image = {0};
    struct bmp_header header = {0};
    if (!error_read_check(from_bmp(file, &header, &image))) {
        file_close(file);
        exit(1);
    };
    file_close(file);

    struct image rotated = rotate_image(image, angle);
    image_destroy(&image);

    if (rotated.data != NULL) {
        FILE *file1 = file_open(args[2], "wb");
        if (!error_write_check(to_bmp(file1, &header, &rotated))) {
            file_close(file);
            exit(1);
        }
        file_close(file1);

        image_destroy(&rotated);
    }
    return 0;
}
