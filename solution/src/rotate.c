#include "../include/image.h"


static void rotate_pixel_0(struct pixel pixel, struct image* new_image, uint32_t i, uint32_t j) {
    set_pixel(new_image, pixel, i, j);
}
static void rotate_pixel_90(struct pixel pixel, struct image* new_image, uint32_t i, uint32_t j) {
    uint32_t x = new_image->height - j - 1;
    uint32_t y = i;
    set_pixel(new_image, pixel, x, y);
}

static void rotate_pixel_180(struct pixel pixel, struct image* new_image, uint32_t i, uint32_t j) {
    uint32_t x = new_image->height - i - 1;
    uint32_t y = new_image->width - j - 1;
    set_pixel(new_image, pixel, x, y);
}

static void rotate_pixel_270(struct pixel pixel, struct image* new_image, uint32_t i, uint32_t j) {
    uint32_t x = j;
    uint32_t y = new_image->width - i - 1;
    set_pixel(new_image, pixel, x, y);
}

static void pixels_foreach(struct image image, struct image* new_image,
        void (*rot_method)(struct pixel pixel, struct image* new_image, uint32_t i, uint32_t j)) {
    for (uint32_t i = 0; i < image.height; i++) {
        for (uint32_t j = 0; j < image.width; j++) {
            rot_method(get_pixel(image, i, j), new_image, i, j);
        }
    }

}

struct image rotate_image(struct image image, int angle) {
    struct image new_image;
    if (angle % 180 == 0) {
        new_image = create_image(image.height, image.width);
        if (new_image.data == NULL)
            return (struct image) {0};
    }
    else
        new_image = create_image(image.width, image.height);
    switch (angle) {
        case 90:
            pixels_foreach(image, &new_image, rotate_pixel_90);
            break;
        case 180:
            pixels_foreach(image, &new_image, rotate_pixel_180);
            break;
        case 270:
            pixels_foreach(image, &new_image, rotate_pixel_270);
            break;
        default:
            pixels_foreach(image, &new_image, rotate_pixel_0);
            break;
    }
    return new_image;
}
